package com.oxidenetwork.dmp_discord;

import org.bukkit.Server;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import email.com.gmail.cosmoconsole.bukkit.deathmsg.DeathMessagePreparedEvent;
import github.scarsz.discordsrv.dependencies.jda.core.EmbedBuilder;

public class DMP_DeathEvent implements Listener {
	public static Server plugin;
	private dmp_discord main;

	public DMP_DeathEvent(dmp_discord main) {
		this.main = main;
	}

	@EventHandler
	public void onDMP_DeathEvent(DeathMessagePreparedEvent event) {
		EmbedBuilder builder = new EmbedBuilder();
		builder.setColor(16711710);
		builder.setAuthor(event.getMessage().toPlainText().substring(4), "http://oxd.io/", "https://crafatar.com/avatars/"+event.getPlayer().getUniqueId().toString());
		main.DiscordFx.sendMessageToDiscord(builder.build());
	}

}
