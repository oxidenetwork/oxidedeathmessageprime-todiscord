package com.oxidenetwork.dmp_discord;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class SettingsManager {
	static FileConfiguration ConfigFile;
	static File pdfile;

	public static void setup() {
		pdfile = new File("plugins/OxideDeathMessagePrime-ToDiscord/", "config.yml");

		if (!pdfile.exists()) {
			try {
				pdfile.createNewFile();
			} catch (IOException e) {
			}
		}

		ConfigFile = YamlConfiguration.loadConfiguration(pdfile);
		LoadDefaults();
	}

	public static FileConfiguration get() {
		return ConfigFile;
	}

	public static void save() {
		try {
			ConfigFile.save(pdfile);
		} catch (IOException e) {
			Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save config.yml!");
		}
	}

	public static void reload() {
		ConfigFile = YamlConfiguration.loadConfiguration(pdfile);
	}

	public static void LoadDefaults() {
		ConfigFile.addDefault("Debug", true);

		ConfigFile.options().copyDefaults(true);
	}

}
