# OxideDeathMessagePrime-ToDiscord
##### TL;DR;
This plugin send the death message from DeathMessagePrime to Discord using the DiscordSRV plugin.

---
Dependecy: 

 * DiscordSRV
 * DeathMessagePrime

Tested on Minecraft: 1.13, 1.14

No configuration is needed. DiscordSRV needs to be setup properly to be able to send messages.

### Permissions
There is only one permission to use the commands
```oxdmp.admin```

### Commands
Main Command: /oxdmp

| Command                  | Description                       |
|--------------------------|-----------------------------------|
| /oxdmp **help**          | Shows the help                    |
| /oxdmp **?**             | Shows the help                    |
| /oxdmp **reload**        | Reloads config                    |
| /oxdmp **save**          | Saves config                      |
| /oxdmp **debug**         | Shows debug state                 |
| /oxdmp **debug** _true_  | Sets debug mode ON                |
| /oxdmp **debug** _false_ | Sets debug mode OFF               |
| /oxdmp **test**          | Send a test message to DiscordSRV |

### Images
![DeathMessage in discord](https://bitbucket.org/oxidenetwork/oxidedeathmessageprime-todiscord/raw/4d3ad7e7983d6d4f00aba4f43687cde61e92b271/Images/DeathMessageInDiscord.PNG)
![Test message](https://bitbucket.org/oxidenetwork/oxidedeathmessageprime-todiscord/raw/4d3ad7e7983d6d4f00aba4f43687cde61e92b271/Images/OXDMP_Testmessage.PNG)