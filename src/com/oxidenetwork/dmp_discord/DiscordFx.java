package com.oxidenetwork.dmp_discord;

import org.bukkit.Server;
import org.bukkit.util.Consumer;

import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.events.DiscordGuildMessageSentEvent;
import github.scarsz.discordsrv.dependencies.jda.core.Permission;
import github.scarsz.discordsrv.dependencies.jda.core.entities.Message;
import github.scarsz.discordsrv.dependencies.jda.core.entities.MessageEmbed;
import github.scarsz.discordsrv.dependencies.jda.core.entities.TextChannel;
import github.scarsz.discordsrv.dependencies.jda.core.exceptions.PermissionException;
import github.scarsz.discordsrv.util.DiscordUtil;

public class DiscordFx {
	public static Server plugin;
	private dmp_discord main;

	public DiscordFx(dmp_discord main) {
		this.main = main;
	}
	
	public void sendMessageToDiscord(String message) {
		if (DiscordSRV.isReady) {
			DiscordSRV discord = DiscordSRV.getPlugin();
			String channel = discord.getMainChatChannel();
			TextChannel textChannel = discord.getDestinationTextChannelForGameChannelName(channel);
			DiscordUtil.sendMessage(textChannel, message);
		} else {
			this.main.Debug("DiscordSRV NOT ready. Message not send.", false);
		}
	}
	
	public void sendMessageToDiscord(MessageEmbed message) {
		if (DiscordSRV.isReady) {
			DiscordSRV discord = DiscordSRV.getPlugin();
			String channel = discord.getMainChatChannel();
			TextChannel textChannel = discord.getDestinationTextChannelForGameChannelName(channel);
			textChannel.sendMessage(message).queue();
		} else {
			main.Debug("DiscordSRV NOT ready. Message not send.", false);
		}
	}
	
    public void sendMessage(TextChannel channel, MessageEmbed message, int expiration, boolean editMessage) {
        if (channel == null) {
        	main.Debug("Tried sending a message to a null channel", false);
            return;
        }

        if (DiscordUtil.getJda() == null) {
        	main.Debug("Tried sending a message using a null JDA instance", false);
            return;
        }

        if (message == null) {
        	main.Debug("Tried sending a null message to " + channel, false);
            return;
        }

        if (message.isEmpty()) {
            main.Debug("Tried sending a blank message to " + channel, false);
            return;
        }

        queueMessage(channel, message, m -> {
            if (expiration > 0) {
                try { Thread.sleep(expiration); } catch (InterruptedException e) { e.printStackTrace(); }
                DiscordUtil.deleteMessage(m);
            }
        });
        
    }
    
    public void sendMessage(MessageEmbed message, int expiration) {
		DiscordSRV discord = DiscordSRV.getPlugin();
		String channel = discord.getMainChatChannel();
		TextChannel textChannel = discord.getDestinationTextChannelForGameChannelName(channel);

    	if (channel == null) {
        	main.Debug("Tried sending a message to a null channel", false);
            return;
        }

        if (DiscordUtil.getJda() == null) {
        	main.Debug("Tried sending a message using a null JDA instance", false);
            return;
        }

        if (message == null) {
        	main.Debug("Tried sending a null message to " + channel, false);
            return;
        }

        if (message.isEmpty()) {
            main.Debug("Tried sending a blank message to " + channel, false);
            return;
        }

        queueMessage(textChannel, message, m -> {
            if (expiration > 0) {
                try { Thread.sleep(expiration); } catch (InterruptedException e) { e.printStackTrace(); }
                DiscordUtil.deleteMessage(m);
            }
        });
        
    }
	
    public  void queueMessage(TextChannel channel, MessageEmbed message) {
        if (channel == null) {
            main.Debug("Tried sending a message to a null channel", false);
            return;
        }
        
        queueMessage(channel, message);
    }    
	
    public void queueMessage(TextChannel channel, MessageEmbed message, Consumer<Message> consumer) {
        if (channel == null) {
        	main.Debug("Tried sending a message to a null channel", false);
            return;
        }

        try {
            channel.sendMessage(message).queue(sendMessage -> {
                DiscordSRV.api.callEvent(new DiscordGuildMessageSentEvent(DiscordUtil.getJda(), sendMessage));
                if (consumer != null) consumer.accept(sendMessage);
            });
        } catch (PermissionException e) {
            if (e.getPermission() != Permission.UNKNOWN) {
                main.Debug("Could not send message in channel " + channel + " because the bot does not have the \"" + e.getPermission().getName() + "\" permission", true);
            } else {
                main.Debug("Could not send message in channel " + channel + " because \"" + e.getMessage() + "\"", true);
            }
        } catch (IllegalStateException ignored) {}
    }
    
	public void sendMessage(TextChannel textChannel, String message) {
		if (DiscordSRV.isReady) {
			DiscordUtil.sendMessage(textChannel, message);
		} else {
			main.Debug("DiscordSRV NOT ready. Message not send.", true);
		}
	}
}
