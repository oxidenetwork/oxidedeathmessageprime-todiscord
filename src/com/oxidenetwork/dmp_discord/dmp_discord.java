package com.oxidenetwork.dmp_discord;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import github.scarsz.discordsrv.dependencies.jda.core.EmbedBuilder;
import net.md_5.bungee.api.ChatColor;

public class dmp_discord extends JavaPlugin  {

	public DiscordFx DiscordFx = new DiscordFx(this);
	private static String chatPrefix = "[" + ChatColor.WHITE + "OX-" + ChatColor.RED + "DMPD" + ChatColor.WHITE + "]";
	private static String baseCommand = "oxdmp";
	private static String pluginName = "OxideDMPtoDSRV";
	
	public static dmp_discord getPlugin() {
		return getPlugin(dmp_discord.class);
	}

	@Override
	public void onEnable() {
		SettingsManager.setup();
		SettingsManager.save();
		registerEvents();
		Bukkit.getLogger().info("Plugin Enabled");
	}

	public void registerEvents() {
		PluginManager pm = Bukkit.getServer().getPluginManager();
		
		boolean stop = false;
		if (pm.getPlugin("DeathMessagesPrime") == null) {
			Debug("DeathMessagePrime not found", true);
			stop = true;
		}
		
		if (pm.getPlugin("DiscordSRV") == null) {
			Debug("DiscordSRV not found", true);
			stop = true;
		}
		
		if (stop) {
			Debug("Required dependencies not found. Disabling plugin. Bye.", true);
			pm.disablePlugin(this);
		}
		
		pm.registerEvents(new DMP_DeathEvent(this), this);		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		if (command.getName().equalsIgnoreCase(baseCommand)) {
			if (sender.hasPermission("oxdmp.admin")) {
				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("reload")) {
						SettingsManager.reload();
						sendChatMessage(sender, ChatColor.YELLOW + "Config Reloaded");
					} else if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
						sendOxHelp(sender);
					} else if (args[0].equalsIgnoreCase("save")) {
						sendChatMessage(sender, "Config file saved");
						SettingsManager.save();
					} else if (args[0].equalsIgnoreCase("test")) {

						EmbedBuilder builder = new EmbedBuilder();
						builder.setAuthor(pluginName, "http://oxd.io", "https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2019/May/30/2209519142-2-oxidedeathmessageprime-todiscord-logo_avatar.png");
						builder.setColor(65290);
						builder.setTitle("OXDMP Test message");
						builder.setDescription("💣   This message will self destruct in 💥 20 seconds.");
						DiscordFx.sendMessage(builder.build(), 20000);
						sendChatMessage(sender, "Test message send. Check Discord.");
						
					} else if (args[0].equalsIgnoreCase("debug")) {
						if (args.length == 2) {
							if (args[1].equalsIgnoreCase("true")) {
								SettingsManager.get().set("Debug", true);
								sendChatMessage(sender, ChatColor.WHITE + "Debug mode set to " + ChatColor.GREEN + "ON");
							} else if (args[1].equalsIgnoreCase("false")) {
								SettingsManager.get().set("Debug", false);
								sendChatMessage(sender, ChatColor.WHITE + "Debug mode set to " + ChatColor.RED + "OFF");
							} else {
								sendChatMessage(sender, ChatColor.RED + "Set to true or false. Example: " + ChatColor.YELLOW + "/" + baseCommand + " debug false");
							}
						} else {
							if (SettingsManager.get().getBoolean("Debug") == true) {
								sendChatMessage(sender, ChatColor.WHITE + "Debug mode is currently " + ChatColor.GREEN + "ON" + ChatColor.WHITE + ". Use " + ChatColor.YELLOW + "/" + baseCommand + " debug false" + ChatColor.WHITE + " to turn it off.");
							} else {
								sendChatMessage(sender, ChatColor.WHITE + "Debug mode is currently " + ChatColor.RED + "OFF" + ChatColor.WHITE +  ". Use " + ChatColor.YELLOW + "/" + baseCommand + " debug true" + ChatColor.WHITE + " to turn it on.");
							}
						}
					}
				} else {
					sendOxHelp(sender);
				}
			} else {
				sendChatMessage(sender, ChatColor.RED + "You don't have permission for this command.");
			}
			return true;
		}
		sendOxHelp(sender);
		return false;
	}
	
	public void sendOxHelp(CommandSender sender) {
		sendChatHeader(sender, "Command Help");
		sender.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW + "/" + baseCommand + " " + ChatColor.GREEN + "?" + ChatColor.RESET
				+ ChatColor.WHITE + " Shows this help.");
		sender.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW + "/" + baseCommand + " " + ChatColor.GREEN + "help"
				+ ChatColor.RESET + ChatColor.WHITE + " Shows this help.");
		sender.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW + "/" + baseCommand + " " + ChatColor.GREEN + "reload"
				+ ChatColor.RESET + ChatColor.WHITE + " Reloads the plugins config and sets the protection again.");
		sender.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW + "/" + baseCommand + " " + ChatColor.GREEN + "save"
				+ ChatColor.RESET + ChatColor.WHITE + " Save the plugin data to the config.");
		sender.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW + "/" + baseCommand + " " + ChatColor.GREEN + "test"
				+ ChatColor.RESET + ChatColor.WHITE + " Send test message to DiscordSRV.");
		sender.sendMessage(ChatColor.ITALIC + "" + ChatColor.YELLOW + "/" + baseCommand + " " + ChatColor.GREEN + "debug"
				+ ChatColor.RESET + ChatColor.WHITE + " Enable or disable debug mode. Use true or false to set.");
	}

	public void sendChatHeader(CommandSender sender, String description) {
		sender.sendMessage(chatPrefix + " - " +pluginName + ChatColor.RESET + " - " + description);
	}
	
	public void sendChatMessage(CommandSender sender, String message) {
		sender.sendMessage(chatPrefix + ChatColor.RESET + " " + message);
		if ((sender instanceof Player)) { // Console send this, don't send another debug line
			Debug(ChatColor.RESET + " " + message, false);
		}
	}
	
	public void Debug(String message, boolean forced) {
		try {
			Boolean debugmode = SettingsManager.get().getBoolean("Debug");
			if (forced == true || debugmode) {
				Bukkit.getConsoleSender().sendMessage(chatPrefix + ChatColor.WHITE + message);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
